var firebaseConfig = {
    apiKey: "AIzaSyC3kpVq2yvOgQjLijtzAnDY-o08OM4_C4A",
    authDomain: "onlinestore-32a86.firebaseapp.com",
    projectId: "onlinestore-32a86",
    storageBucket: "onlinestore-32a86.appspot.com",
    messagingSenderId: "247135709517",
    appId: "1:247135709517:web:701a07624828a973876f04",
    measurementId: "G-HGFM81CB0R"
};
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();
  //global
var productos=[];
var cartItems=[];
var cart_n = document.getElementById('cart_n');

//divs
var compDIV= document.getElementById("Camarones");
var accesorioDIV= document.getElementById("Pescados");
//var acercaDIV = Document.getElementById("acercaDiv");
/// informacion
var CAMA = [ {name: 'Camarones apanados',price:8}];
    
var PESCA =[
    {name: 'Disco Duro',price:90,cantidad:1},
    {name: 'Cargador',price:50,cantidad:1},
    {name: 'Pendrive',price:12,cantidad:1},
    {name: 'Adaptador',price:15,cantidad:1}];
//var ACERCA =[];*/

///HTML
function HTMLpcProduct(con){
    let URL = `img/camarones/cam${con}.jpg`;
    let btn = `btnComp${con}`;
    return `
     <div class="col-md-4">
        <div class="card mb-4 shadow-sm">
                 <img class= "card-img-top" style="height:16rem;" src="${URL}"
                   alt="Card image cap"></img>
               <div class="card-body">
                    <i style="color:orange;" class="fa fa-star"></i>   
                    <i style="color:orange;" class="fa fa-star"></i>   
                    <i style="color:orange;" class="fa fa-star"></i>   
                    <i style="color:orange;" class="fa fa-star"></i>   
                    <p class="card-text">${CAMA[con- 1].name}</p>                
                    <p class="card-text">Precio: $${CAMA[con-1].price}.00</p>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <button type="button" onclick="cart2('${CAMA[con-1].name}',
                            '${CAMA[con-1].price}', '${URL}','${con}','${btn}')"
                            class="btn btn-sm btn-outline-secondary"><a style="color:inherit;">Buy</button>
                            <button id='${con}' type="button" onclick="cart('${CAMA[con-1].name}',
                            '${CAMA[con-1].price}','${URL}','${con}','${btn}')"
                            class="btn btn-sm btn-outline-secondary">Add to cart</button>
                        </div>
                        <small class="text-muted">Free shipping</small>                    
                    </div>
                </div>               
        </div>
     </div>
    `
}
function HTMLartProduct(con){
    let URL = `images/accesorios/art${con}.jpg`;
    let btn = `btnAcces${con}`;
    return `
     <div class="col-md-4">
        <div class="card mb-4 shadow-sm">
                 <img class= "card-img-top" style="height:16rem;" src="${URL}"
                   alt="Card image cap"></img>
               <div class="card-body">
                    <i style="color:orange;" class="fa fa-star"></i>   
                    <i style="color:orange;" class="fa fa-star"></i>   
                    <i style="color:orange;" class="fa fa-star"></i>   
                    <i style="color:orange;" class="fa fa-star"></i>   
                    <p class="card-text">${ACCESORIO[con-1].name}</p>                
                    <p class="card-text">Preccio: $${ACCESORIO[con-1].price}.00</p>
                    <p class="card-text">cantidad: ${ACCESORIO[con-1].cantidad}</p>
                    <div class="d-flex justify-content-between align-items-center">
                        <div  class="btn-group">
                            <button type="button" onclick="cart2(
                                '${ACCESORIO[con-1].name}','${ACCESORIO[con-1].price}','${ACCESORIO[con-1].cantidad}','${URL}','${con}','${btn}')"
                            class="btn btn-sm btn-outline-secondary"><a style="color:inherit;">Buy</button>
                            <button id="${con}" type="button" onclick="cart('${ACCESORIO[con-1].name}',
                            '${ACCESORIO[con-1].price}','${ACCESORIO[con-1].cantidad}','${URL}','${con}','${btn}')"
                            class="btn btn-sm btn-outline-secondary">Add to cart</button>
                        </div>
                        <small class="text-muted">Free shipping</small>                    
                    </div>
                </div>               
        </div>
     </div>
    `
}

function animation(){
    const toast= swal.mixin({
        toast:true,
        position:'top-end',
        showConfirmButton:false,
        timer:1000
    });

    toast({
        type:'success',
        title: 'Added to shopping cart'
    });
}
//cart function
function cart(name,price,cantidad,url,con,btncart){
    var item={
        name:name,
        price:price,
        cantidad:cantidad,
        url:url
    }
    cartItems.push(item);
    let storage= JSON.parse(localStorage.getItem("cart"));
    if(storage==null){
        productos.push(item);
        localStorage.setItem("cart",JSON.stringify(productos));
    }else{
        productos= JSON.parse(localStorage.getItem("cart"));
        productos.push(item);
        localStorage.setItem("cart",JSON.stringify(productos));
    }
    productos= JSON.parse(localStorage.getItem("cart"));
    cart_n.innerHTML=`[${productos.length}]`;
    document.getElementById(con).style.display="none";
    // document.getElementById(btncart).style.display="none";
    animation();
}

function cart2(name,price,cantidad,url,con,btncart){
    var item={
        name:name,
        price:price,
        cantidad:cantidad,
        url:url
    }
    cartItems.push(item);
    let storage= JSON.parse(localStorage.getItem("cart"));
    if(storage==null){
        productos.push(item);
        localStorage.setItem("cart",JSON.stringify(productos));
    }else{
        productos= JSON.parse(localStorage.getItem("cart"));
        productos.push(item);
        localStorage.setItem("cart",JSON.stringify(productos));
    }
    productos= JSON.parse(localStorage.getItem("cart"));
    cart_n.innerHTML=`[${productos.length}]`;
    document.getElementById(con).style.display="none";
    // document.getElementById(btncart).style.display="none";

}

/// RENDER

function render(){
    for(let index = 1; index <= 4; index++){
        compDIV.innerHTML+= `${HTMLpcProduct(index)}`;
    }
    for(let index = 1; index <= 4 ; index++){
        accesorioDIV.innerHTML+=`${HTMLartProduct(index)}`;
    }
    if(localStorage.getItem("cart")==null){
    }else{
        productos=JSON.parse(localStorage.getItem("cart"));
        cart_n.innerHTML=`[${productos.length}]`;
    }
}
